from django.db import models

# Create your models here.
class Content(models.Model):
    key=models.CharField(max_length=100)
    value=models.TextField()

    def __str__(self):
        return self.key


class Comentario(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    cuerpo = models.TextField(blank=False)
    fecha = models.DateTimeField('publicado')