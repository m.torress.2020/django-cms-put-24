from django.urls import path
from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path('main.css', views.style),
    path("<llave>", views.get_content),
]