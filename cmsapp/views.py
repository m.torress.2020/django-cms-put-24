from django.shortcuts import render
from django.http import HttpResponse
from .models import Content
from django.views.decorators.csrf import csrf_exempt
# Create your views here.

response = """
body {
    margin: 10px 20% 50px 70px;
    font-family: sans-serif;
    color: orange;
    background: blue;
}"""

@csrf_exempt
def get_content(request, llave):
    if request.method == 'PUT':
        value = request.body.decode('utf-8')
        try:
            c = Content.objects.get(key=llave)
            c.value = value
        except Content.DoesNotExist:
            c = Content(key=llave, value=value)
        c.save()
        return HttpResponse("Content saved successfully")

    elif request.method == 'GET':
        try:
            c = Content.objects.get(key=llave)
            response_data = f"Key: {c.key}, Value: {c.value}"
            return HttpResponse(response_data)
        except Content.DoesNotExist:
            return HttpResponse("Content not found", status=404)

    elif request.method == 'POST':
        llave = request.POST.get('key')
        value = request.POST.get('value')
        try:
            c = Content.objects.get(key=llave)
            c.value = value
        except Content.DoesNotExist:
            c = Content(key=llave, value=value)
        c.save()
        return HttpResponse("Content saved successfully")

def index(request):
    content_list = Content.objects.all()[:5]
    content = {'content_list': content_list}
    return render(request, 'index.html', {'contenido': content})

def style(request):
    return HttpResponse(response, content_type="text/css")